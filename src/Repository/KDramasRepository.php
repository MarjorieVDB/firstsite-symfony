<?php

namespace App\Repository;

use App\Entity\KDramas;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method KDramas|null find($id, $lockMode = null, $lockVersion = null)
 * @method KDramas|null findOneBy(array $criteria, array $orderBy = null)
 * @method KDramas[]    findAll()
 * @method KDramas[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class KDramasRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, KDramas::class);
    }

    // /**
    //  * @return KDramas[] Returns an array of KDramas objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('k')
            ->andWhere('k.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('k.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?KDramas
    {
        return $this->createQueryBuilder('k')
            ->andWhere('k.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
