<?php

namespace App\Entity;

use App\Repository\ActeursRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ActeursRepository::class)
 */
class Acteurs
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $nationalite;

    /**
     * @ORM\ManyToMany(targetEntity=KDramas::class, mappedBy="acteurs_id_acteurs")
     */
    private $kDramas;

    public function __construct()
    {
        $this->kDramas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getNationalite(): ?string
    {
        return $this->nationalite;
    }

    public function setNationalite(string $nationalite): self
    {
        $this->nationalite = $nationalite;

        return $this;
    }

    /**
     * @return Collection|KDramas[]
     */
    public function getKDramas(): Collection
    {
        return $this->kDramas;
    }

    public function addKDrama(KDramas $kDrama): self
    {
        if (!$this->kDramas->contains($kDrama)) {
            $this->kDramas[] = $kDrama;
            $kDrama->addActeursIdActeur($this);
        }

        return $this;
    }

    public function removeKDrama(KDramas $kDrama): self
    {
        if ($this->kDramas->removeElement($kDrama)) {
            $kDrama->removeActeursIdActeur($this);
        }

        return $this;
    }
}
