<?php

namespace App\Entity;

use App\Repository\KDramasRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=KDramasRepository::class)
 */
class KDramas
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $nom;

    /**
     * @ORM\Column(type="integer")
     */
    private $episodes;

    /**
     * @ORM\Column(type="integer")
     */
    private $annee;

    /**
     * @ORM\ManyToMany(targetEntity=acteurs::class, inversedBy="kDramas")
     */
    private $acteurs_id_acteurs;

    public function __construct()
    {
        $this->acteurs_id_acteurs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getEpisodes(): ?int
    {
        return $this->episodes;
    }

    public function setEpisodes(int $episodes): self
    {
        $this->episodes = $episodes;

        return $this;
    }

    public function getAnnee(): ?int
    {
        return $this->annee;
    }

    public function setAnnee(int $annee): self
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * @return Collection|acteurs[]
     */
    public function getActeursIdActeurs(): Collection
    {
        return $this->acteurs_id_acteurs;
    }

    public function addActeursIdActeur(acteurs $acteursIdActeur): self
    {
        if (!$this->acteurs_id_acteurs->contains($acteursIdActeur)) {
            $this->acteurs_id_acteurs[] = $acteursIdActeur;
        }

        return $this;
    }

    public function removeActeursIdActeur(acteurs $acteursIdActeur): self
    {
        $this->acteurs_id_acteurs->removeElement($acteursIdActeur);

        return $this;
    }
}
