<?php

namespace App\Form;

use App\Entity\KDramas;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class KdramasType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('episodes')
            ->add('annee')
            ->add('acteurs_id_acteurs')
            ->add('valider')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => KDramas::class,
        ]);
    }
}
