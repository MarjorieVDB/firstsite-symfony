<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210506130829 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE kdramas_acteurs (kdramas_id INT NOT NULL, acteurs_id INT NOT NULL, INDEX IDX_724E49B9F804C7CC (kdramas_id), INDEX IDX_724E49B971A27AFC (acteurs_id), PRIMARY KEY(kdramas_id, acteurs_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE kdramas_acteurs ADD CONSTRAINT FK_724E49B9F804C7CC FOREIGN KEY (kdramas_id) REFERENCES kdramas (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE kdramas_acteurs ADD CONSTRAINT FK_724E49B971A27AFC FOREIGN KEY (acteurs_id) REFERENCES acteurs (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE acteurs DROP age');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE kdramas_acteurs');
        $this->addSql('ALTER TABLE acteurs ADD age INT NOT NULL');
    }
}
